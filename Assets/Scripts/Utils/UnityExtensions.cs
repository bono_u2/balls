﻿using UnityEngine;

public static class UnityExtensions
{
    public static T SafeDestroy<T> (this T _object) where T : Object
    {
        if (_object != null)
        {

            if (Application.isEditor && !Application.isPlaying)
                Object.DestroyImmediate (_object);
            else
                Object.Destroy (_object);
        }

        return _object;
    }

    public static float GetMaxTime (this AnimationCurve _curve)
    {
        return _curve.keys.Length > 0 ? _curve.keys[_curve.length - 1].time : 0f;
    }
}