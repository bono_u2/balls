﻿using UnityEngine;

namespace Utils
{
    public class TextureUtils
    {
        #region Public static vars
        public static Texture2D BLACK_CLEAR_TEXTURE_4 = CreateTexture (4, 4, Colors.blackclear);
        #endregion

        #region Public static methods

        #region Merge textures
        public static void MergeTexture (ref Texture2D _baseTexture, Texture2D _compositeTexture, int _x = 0, int _y = 0)
        {
            MergeTexture (ref _baseTexture, _compositeTexture, new Rect (0, 0, _baseTexture.width, _baseTexture.height), _x, _y);
        }

        public static void MergeTexture (ref Texture2D _baseTexture, Texture2D _compositeTexture, Rect _cropRect, int _x = 0, int _y = 0)
        {
            if (_baseTexture == null || _compositeTexture == null)
                return;

            int cropX = (int)_cropRect.x;
            int cropY = (int)_cropRect.y;
            int cropW = (int)_cropRect.width;
            int cropH = (int)_cropRect.height;

            for (int i = 0; i < _compositeTexture.width; i++)
            {
                for (int j = 0; j < _compositeTexture.height; j++)
                {
                    Color compositeColor = _compositeTexture.GetPixel (i, j);

                    int pX = i + _x;
                    int pY = j + _y;

                    if (pX >= cropX && pY >= cropY && (pX < (cropW + cropX)) && (pY < (cropH + cropY)))
                    {
                        Color baseColor = _baseTexture.GetPixel (i + _x, j + _y);
                        Color newColor = Color.Lerp (baseColor, compositeColor, compositeColor.a);
                        _baseTexture.SetPixel (i + _x, j + _y, newColor);
                    }
                }
            }
        }

        public static void MergeTextureBlit (Texture2D _baseTexture, Texture2D _compositeTexture, string _shaderName, Rect _rect)
        {
            float w = _baseTexture.width;
            float h = _baseTexture.height;

            Vector2 tiling = new Vector2 (w / _rect.width, h / _rect.height);
            Vector2 offset = new Vector2 (-_rect.x / w * tiling.x, -_rect.y / h * tiling.y);

            Matrix4x4 matrix = Matrix4x4.TRS (offset, Quaternion.identity, tiling);

            Shader shader = Shader.Find (_shaderName);
            Material shaderMaterial = new Material (shader);
            shaderMaterial.SetTexture ("_CombTex", _compositeTexture);
            shaderMaterial.SetMatrix ("_RotateMatrix", matrix);

            var tmpRT = RenderTexture.active;

            RenderTexture rt = new RenderTexture (_baseTexture.width, _baseTexture.height, 0, RenderTextureFormat.ARGB32);
            rt.wrapMode = TextureWrapMode.Clamp;
            rt.DiscardContents ();

            RenderTexture.active = rt;

            GL.Clear (true, true, Color.clear);

            Graphics.Blit (_baseTexture, rt, shaderMaterial);

            _baseTexture.ReadPixels (new Rect (0f, 0f, rt.width, rt.height), 0, 0);
            _baseTexture.Apply ();

            RenderTexture.active = tmpRT;

            Object.Destroy (rt);
            Object.Destroy (shaderMaterial);
        }

        public static void MergeTextureBlit (Texture2D _baseTexture, Texture2D _compositeTexture, string _shaderName)
        {
            Shader sh = Shader.Find (_shaderName);
            Material shaderMaterial = new Material (sh);
            shaderMaterial.SetTexture ("_CombTex", _compositeTexture);

            var tmpRT = RenderTexture.active;

            RenderTexture rt = new RenderTexture (_baseTexture.width, _baseTexture.height, 0, RenderTextureFormat.ARGB32);
            rt.wrapMode = TextureWrapMode.Clamp;
            rt.DiscardContents ();

            RenderTexture.active = rt;

            GL.Clear (true, true, Color.clear);

            Graphics.Blit (_baseTexture, rt, shaderMaterial);

            _baseTexture.ReadPixels (new Rect (0f, 0f, rt.width, rt.height), 0, 0);
            _baseTexture.Apply ();

            RenderTexture.active = tmpRT;

            Object.Destroy (rt);
            Object.Destroy (shaderMaterial);
        }
        #endregion

        #region Create texture
        public static Texture2D CreateTexture (int _width, int _height, Color _color, bool _mipmap = false, bool _linear = true)
        {
            return CreateTexture (_width, _height, TextureFormat.ARGB32, _color, _mipmap, _linear);
        }

        public static Texture2D CreateTexture (int _width, int _height, TextureFormat _format, Color _color, bool _mipmap = false, bool _linear = true)
        {
            Texture2D texture = new Texture2D (_width, _height, _format, _mipmap, _linear);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Trilinear;
            texture.anisoLevel = 4;

            Color[] destinationPixels = new Color[(_width * _height)];

            for (int i = 0; i < destinationPixels.Length; i++)
                destinationPixels[i] = _color;

            texture.SetPixels (0, 0, _width, _height, destinationPixels);
            texture.Apply ();

            return texture;
        }

        public static Texture2D CreateMask (float _width, float _height, Rect _maskUVRect, float _ratio)
        {
            var width = _width * _ratio;
            var height = _height * _ratio;

            var blankTex = TextureUtils.CreateTexture ((int)(width), (int)(height), Color.clear);
            var cropRect = new Rect (0f, 0f, width, height);

            var mainTex = Texture2D.Instantiate (blankTex);
            Texture2D maskTex = TextureUtils.CreateTexture ((int)(width * _maskUVRect.width), (int)(height * _maskUVRect.height), Color.black);
            maskTex.wrapMode = TextureWrapMode.Clamp;
            maskTex.filterMode = FilterMode.Bilinear;
            maskTex.anisoLevel = 1;

            TextureUtils.MergeTexture (ref mainTex, maskTex, cropRect, (int)(width * _maskUVRect.x), (int)(width * _maskUVRect.y));
            mainTex.Apply ();

            return mainTex;
        }

        public static Texture2D CreateWarningTexture (int _width, int _height, string _messageWarning = "Texture not found! Created a temporary texture.")
        {
            var color = Colors.orange;
            var texture = CreateTexture (_width, _height, color);

            if (!string.IsNullOrEmpty (_messageWarning))
                Console.Warning (_messageWarning);

            return texture;
        }
        #endregion

        #region Adjustment
        public static Texture2D InvertAlpha (Texture2D _original)
        {
            Texture2D tex = new Texture2D (_original.width, _original.height);

            for (int i = 0; i < _original.width; i++)
            {
                for (int j = 0; j < _original.height; j++)
                {
                    var pixel = _original.GetPixel (i, j);
                    pixel.a = 1 - pixel.a;
                    tex.SetPixel (i, j, pixel);
                }
            }

            return tex;
        }
        #endregion

        #region Transform
        public static Texture2D FlipYTexture (Texture2D _original)
        {
            Texture2D flipped = new Texture2D (_original.width, _original.height);

            int xN = _original.width;
            int yN = _original.height;

            for (int i = 0; i < xN; i++)
            {
                for (int j = 0; j < yN; j++)
                {
                    flipped.SetPixel (i, yN - j - 1, _original.GetPixel (i, j));
                }
            }

            return flipped;
        }

        public static Texture2D FlipXTexture (Texture2D _original)
        {
            Texture2D flipped = new Texture2D (_original.width, _original.height);

            int xN = _original.width;
            int yN = _original.height;

            for (int i = 0; i < xN; i++)
            {
                for (int j = 0; j < yN; j++)
                {
                    flipped.SetPixel (xN - i - 1, j, _original.GetPixel (i, j));
                }
            }

            return flipped;
        }
        #endregion

        #endregion
    }
}