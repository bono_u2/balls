using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Utils
{
    public class MathUtils
    {
        #region Random
        public static int RandRange (int min, int max)
        {
            return Random.Range (min, max);
        }

        public static float RandRange (float min, float max)
        {
            return Random.Range (min, max);
        }

        public static float RandomValue ()
        {
            return Random.value;
        }

        public static bool RandomBool (int _trueRation = 50, int _falseRatio = 50)
        {
            bool result = false;
            System.Random rnd = new System.Random ();
            if (rnd.Next (_trueRation + _falseRatio) < _trueRation)
                result = true;

            return result;
        }

        public static int GetIndexByChance (List<int> _chances)
        {
            int sum = _chances.Sum (x => x);
            var rnd = RandRange (0, sum);

            int result = 0;

            int currentSum = 0;
            for (int j = 0; j < _chances.Count; j++)
            {
                if (currentSum <= rnd && rnd < currentSum + _chances[j])
                {
                    result = j;
                    break;
                }

                currentSum += _chances[j];
            }

            return result;
        }
        #endregion

        #region Angles
        public static float LimitAngle (float angle, float min, float max)
        {
            min = ClampAngle (min + 360);
            max = ClampAngle (max + 360);

            var addition = 0f;
            if (max < min)
            {
                addition = max;
                max = 360;
                min -= addition;
                angle -= addition;
                if (angle < 0)
                {
                    angle += 360;
                }
            }
            return Mathf.Clamp (angle, min, max) + addition;
        }

        public static float ClampAngle (float angle, float min, float max)
        {
            angle = ClampAngle (angle);

            return Mathf.Clamp (angle, min, max);
        }

        public static float ClampAngle (float angle)
        {
            while (angle < 0f)
                angle += 360f;

            while (angle > 360f)
                angle -= 360f;

            return angle;
        }

        /// <summary>
        /// ������ ���� ���� ����� ����� ���������
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static float GetAngleSigned (Vector3 from, Vector3 to)
        {
            var angle = Vector3.Angle (from, to);
            var cross = Vector3.Cross (from, to);
            if (cross.y < 0) angle = -angle;

            return angle;
        }

        // example: float angle = GetAngleSigned (end - start, Vector3.left, Vector3.back);
        public static float GetAngleSigned (Vector3 v1, Vector3 v2, Vector3 normal)
        {
            return Mathf.Atan2 (Vector3.Dot (normal, Vector3.Cross (v1, v2)), Vector3.Dot (v1, v2)) * Mathf.Rad2Deg;
        }

        public static float GetAngle (Vector3 fromV, Vector3 toV1, Vector3 toV2)
        {
            Vector3 p1 = toV1 - fromV;
            float a1 = Mathf.Atan2 (p1.z, p1.x);
            if (a1 < 0f)
                a1 += Mathf.PI * 2f;

            Vector3 p2 = toV2 - fromV;
            float a2 = Mathf.Atan2 (p2.z, p2.x);
            if (a2 < 0f)
                a2 += Mathf.PI * 2f;

            float delta = a2 - a1;
            if (delta > Mathf.PI)
                delta = delta - Mathf.PI * 2f;

            if (delta < -Mathf.PI)
                delta = Mathf.PI * 2f + delta;

            return delta;
        }

        public static float EllipseRadiusAtAngle (float angle, float radiusA, float radiusB)
        {
            return radiusA * radiusB / Mathf.Sqrt (
                radiusA * radiusA * Mathf.Sin (angle * Mathf.Deg2Rad) * Mathf.Sin (angle * Mathf.Deg2Rad) +
                radiusB * radiusB * Mathf.Cos (angle * Mathf.Deg2Rad) * Mathf.Cos (angle * Mathf.Deg2Rad));
        }
        #endregion

        #region Gaussian

        public static float Gaussian (float x, float mu, float sigma)
        {
            float exp = Mathf.Exp (-(Mathf.Pow (x - mu, 2f) / (2f * Mathf.Pow (sigma, 2f))));

            return (1f / (sigma * Mathf.Sqrt (2f * Mathf.PI))) * exp;
        }

        #endregion
    }
}