﻿using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;

public class MeshSerializer
{
    #region Public static methods
    public static Mesh ReadMesh (byte[] bytes)
    {
        if (bytes == null || bytes.Length < 5)
            throw new Exception ("Invalid mesh file!");

        var buf = new BinaryReader (new MemoryStream (bytes));

        // read header
        var vertCount = buf.ReadUInt16 ();
        var triCount = buf.ReadUInt16 ();
        var format = buf.ReadByte ();

        // sanity check
        if (vertCount < 0 || vertCount > 64000)
            throw new Exception ("Invalid vertex count in the mesh data!");
        if (triCount < 0 || triCount > 64000)
            throw new Exception ("Invalid triangle count in the mesh data!");
        if (format < 1 || (format & 1) == 0 || format > 63)
            throw new Exception ("Invalid vertex format in the mesh data!");

        Mesh mesh = new Mesh ();

        // positions
        var verts = new Vector3[vertCount];
        ReadVector3Array16Bit (verts, buf);
        mesh.vertices = verts;

        if ((format & 2) != 0) // have normals
        {
            var normals = new Vector3[vertCount];
            ReadVector3ArrayBytes (normals, buf);
            mesh.normals = normals;
        }

        if ((format & 4) != 0) // have tangents
        {
            var tangents = new Vector4[vertCount];
            ReadVector4ArrayBytes (tangents, buf);
            mesh.tangents = tangents;
        }

        if ((format & 8) != 0) // have UVs
        {
            var uvs = new Vector2[vertCount];
            ReadVector2Array16Bit (uvs, buf);
            mesh.uv = uvs;
        }

        if ((format & 16) != 0) // have UVs2
        {
            var uvs2 = new Vector2[vertCount];
            ReadVector2Array16Bit (uvs2, buf);
            mesh.uv2 = uvs2;
        }

        if ((format & 32) != 0) // have Colors
        {
            var colors = new Color[vertCount];
            ReadColorArray16Bit (colors, buf);
            mesh.colors = colors;
        }

        // triangle indices
        var tris = new int[triCount * 3];
        for (int i = 0; i < triCount; ++i)
        {
            tris[i * 3 + 0] = buf.ReadUInt16 ();
            tris[i * 3 + 1] = buf.ReadUInt16 ();
            tris[i * 3 + 2] = buf.ReadUInt16 ();
        }
        mesh.triangles = tris;

        buf.Close ();

        return mesh;
    }

    public static void ReadMesh (byte[] bytes, ref Mesh mesh)
    {
        if (bytes == null || bytes.Length < 5)
            throw new Exception ("Invalid mesh file!");

        var buf = new BinaryReader (new MemoryStream (bytes));

        // read header
        var vertCount = buf.ReadUInt16 ();
        var triCount = buf.ReadUInt16 ();
        var format = buf.ReadByte ();

        // sanity check
        if (vertCount < 0 || vertCount > 64000)
            throw new Exception ("Invalid vertex count in the mesh data!");
        if (triCount < 0 || triCount > 64000)
            throw new Exception ("Invalid triangle count in the mesh data!");
        if (format < 1 || (format & 1) == 0 || format > 63)
            throw new Exception ("Invalid vertex format in the mesh data!");

        // positions
        var verts = new Vector3[vertCount];
        ReadVector3Array16Bit (verts, buf);
        mesh.vertices = verts;

        if ((format & 2) != 0) // have Normals
        {
            var normals = new Vector3[vertCount];
            ReadVector3ArrayBytes (normals, buf);
            mesh.normals = normals;
        }

        if ((format & 4) != 0) // have Tangents
        {
            var tangents = new Vector4[vertCount];
            ReadVector4ArrayBytes (tangents, buf);
            mesh.tangents = tangents;
        }

        if ((format & 8) != 0) // have UVs
        {
            var uvs = new Vector2[vertCount];
            ReadVector2Array16Bit (uvs, buf);
            mesh.uv = uvs;
        }

        if ((format & 16) != 0) // have UVs
        {
            var uvs2 = new Vector2[vertCount];
            ReadVector2Array16Bit (uvs2, buf);
            mesh.uv2 = uvs2;
        }

        if ((format & 32) != 0) // have Colors
        {
            var colors = new Color[vertCount];
            ReadColorArray16Bit (colors, buf);
            mesh.colors = colors;
        }

        // triangle indices
        var tris = new int[triCount * 3];
        for (var i = 0; i < triCount; ++i)
        {
            tris[i * 3 + 0] = buf.ReadUInt16 ();
            tris[i * 3 + 1] = buf.ReadUInt16 ();
            tris[i * 3 + 2] = buf.ReadUInt16 ();
        }
        mesh.triangles = tris;

        buf.Close ();
    }

    public static byte[] WriteMesh (Mesh mesh, bool saveTangents)
    {
        if (!mesh)
            throw new Exception ("No mesh given!");

        var verts = mesh.vertices;
        var normals = mesh.normals;
        var tangents = mesh.tangents;
        var uvs = mesh.uv;
        var uvs2 = mesh.uv2;
        var tris = mesh.triangles;
        var colors = mesh.colors;

        // figure out vertex format
        byte format = 1;
        if (normals.Length > 0)
            format |= 2;
        if (saveTangents && tangents.Length > 0)
            format |= 4;
        if (uvs.Length > 0)
            format |= 8;
        if (uvs2.Length > 0)
            format |= 16;
        if (colors.Length > 0)
            format |= 32;

        var stream = new MemoryStream ();
        var buf = new BinaryWriter (stream);

        // write header
        var vertCount = (ushort)verts.Length;
        var triCount = (ushort)(tris.Length / 3);
        buf.Write (vertCount);
        buf.Write (triCount);
        buf.Write (format);

        // vertex components
        WriteVector3Array16Bit (verts, buf);
        WriteVector3ArrayBytes (normals, buf);
        if (saveTangents)
            WriteVector4ArrayBytes (tangents, buf);
        WriteVector2Array16Bit (uvs, buf);
        WriteVector2Array16Bit (uvs2, buf);
        WriteColorArray16Bit (colors, buf);

        // triangle indices
        foreach (var idx in tris)
        {
            var idx16 = (ushort)idx;
            buf.Write (idx16);
        }
        buf.Close ();

        return stream.ToArray ();
    }

    public static Dictionary<string, Mesh> ReadMeshes (byte[] bytes)
    {
        if (bytes == null || bytes.Length < 5)
            throw new Exception ("Invalid mesh file!");

        var meshes = new Dictionary<string, Mesh> ();

        var stream = new MemoryStream (bytes);
        var buf = new BinaryReader (stream);

        var meshesCount = buf.ReadUInt16 ();

        for (var i = 0; i < meshesCount; i++)
        {
            var meshType = buf.ReadString ();
            var meshByteLength = buf.ReadInt32 ();
            var meshBytes = buf.ReadBytes (meshByteLength);

            var mesh = ReadMesh (meshBytes);

            meshes.Add (meshType, mesh);
        }

        buf.Close ();

        return meshes;
    }

    public static byte[] WriteMeshes (Dictionary<string, Mesh> meshes)
    {
        if (meshes == null || meshes.Count <= 0)
            throw new Exception ("No mesh given!");

        var stream = new MemoryStream ();
        var buf = new BinaryWriter (stream);

        buf.Write ((ushort)meshes.Count);

        var keys = meshes.Keys;

        foreach (var item in meshes)
        {
            var mesh = item.Value;
            if (mesh == null || mesh.vertices.Length <= 0)
                continue;

            var meshType = item.Key;
            var meshBytes = WriteMesh (mesh, false);
            var meshByteLength = meshBytes.Length;

            buf.Write (meshType);
            buf.Write (meshByteLength);
            buf.Write (meshBytes);
        }

        buf.Close ();

        return stream.ToArray ();
    }

    public static void ReadVector3Array16Bit (Vector3[] arr, BinaryReader buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        // read bounding box
        Vector3 bmin;
        Vector3 bmax;
        bmin.x = buf.ReadSingle ();
        bmax.x = buf.ReadSingle ();
        bmin.y = buf.ReadSingle ();
        bmax.y = buf.ReadSingle ();
        bmin.z = buf.ReadSingle ();
        bmax.z = buf.ReadSingle ();

        // decode vectors as 16 bit integer components between the bounds
        for (var i = 0; i < length; ++i)
        {
            ushort ix = buf.ReadUInt16 ();
            ushort iy = buf.ReadUInt16 ();
            ushort iz = buf.ReadUInt16 ();
            float xx = ix / 65535.0f * (bmax.x - bmin.x) + bmin.x;
            float yy = iy / 65535.0f * (bmax.y - bmin.y) + bmin.y;
            float zz = iz / 65535.0f * (bmax.z - bmin.z) + bmin.z;
            arr[i] = new Vector3 (xx, yy, zz);
        }
    }

    public static void WriteVector3Array16Bit (Vector3[] arr, BinaryWriter buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        // calculate bounding box of the array
        var bounds = new Bounds (arr[0], new Vector3 (0.001f, 0.001f, 0.001f));
        for (var i = 0; i < length; ++i)
            bounds.Encapsulate (arr[i]);

        //Debug.Log (bounds.center.ToString () + "   " + bounds.size.ToString ());

        // write bounds to stream
        var bmin = bounds.min;
        var bmax = bounds.max;
        buf.Write (bmin.x);
        buf.Write (bmax.x);
        buf.Write (bmin.y);
        buf.Write (bmax.y);
        buf.Write (bmin.z);
        buf.Write (bmax.z);

        // encode vectors as 16 bit integer components between the bounds
        for (var i = 0; i < length; ++i)
        {
            var v = arr[i];

            var xx = Mathf.Clamp ((v.x - bmin.x) / (bmax.x - bmin.x) * 65535.0f, 0.0f, 65535.0f);
            var yy = Mathf.Clamp ((v.y - bmin.y) / (bmax.y - bmin.y) * 65535.0f, 0.0f, 65535.0f);
            var zz = Mathf.Clamp ((v.z - bmin.z) / (bmax.z - bmin.z) * 65535.0f, 0.0f, 65535.0f);
            var ix = (ushort)xx;
            var iy = (ushort)yy;
            var iz = (ushort)zz;
            buf.Write (ix);
            buf.Write (iy);
            buf.Write (iz);
        }
    }

    public static void WriteVector3Array16Bit (Bounds _bounds, Vector3[] arr, BinaryWriter buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        //Debug.Log (_bounds.center.ToString () + "   " + _bounds.size.ToString ());

        // write bounds to stream
        var bmin = _bounds.min;
        var bmax = _bounds.max;
        buf.Write (bmin.x);
        buf.Write (bmax.x);
        buf.Write (bmin.y);
        buf.Write (bmax.y);
        buf.Write (bmin.z);
        buf.Write (bmax.z);

        // encode vectors as 16 bit integer components between the bounds
        for (var i = 0; i < length; ++i)
        {
            var v = arr[i];

            var xx = Mathf.Clamp ((v.x - bmin.x) / (bmax.x - bmin.x) * 65535.0f, 0.0f, 65535.0f);
            var yy = Mathf.Clamp ((v.y - bmin.y) / (bmax.y - bmin.y) * 65535.0f, 0.0f, 65535.0f);
            var zz = Mathf.Clamp ((v.z - bmin.z) / (bmax.z - bmin.z) * 65535.0f, 0.0f, 65535.0f);
            var ix = (ushort)xx;
            var iy = (ushort)yy;
            var iz = (ushort)zz;
            buf.Write (ix);
            buf.Write (iy);
            buf.Write (iz);
        }
    }

    public static void ReadVector3Array32Bit (Vector3[] arr, BinaryReader buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        for (var i = 0; i < length; ++i)
        {
            float x = buf.ReadSingle ();
            float y = buf.ReadSingle ();
            float z = buf.ReadSingle ();
            arr[i] = new Vector3 (x, y, z);
        }
    }

    public static void WriteVector3Array32Bit (Vector3[] arr, BinaryWriter buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        for (var i = 0; i < length; ++i)
        {
            var v = arr[i];
            buf.Write (v.x);
            buf.Write (v.y);
            buf.Write (v.z);
        }
    }

    public static void ReadVector2Array16Bit (Vector2[] arr, BinaryReader buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        // Read bounding box
        Vector2 bmin;
        Vector2 bmax;
        bmin.x = buf.ReadSingle ();
        bmax.x = buf.ReadSingle ();
        bmin.y = buf.ReadSingle ();
        bmax.y = buf.ReadSingle ();

        // Decode vectors as 16 bit integer components between the bounds
        for (var i = 0; i < length; ++i)
        {
            ushort ix = buf.ReadUInt16 ();
            ushort iy = buf.ReadUInt16 ();
            float xx = ix / 65535.0f * (bmax.x - bmin.x) + bmin.x;
            float yy = iy / 65535.0f * (bmax.y - bmin.y) + bmin.y;
            arr[i] = new Vector2 (xx, yy);
        }
    }

    public static void WriteVector2Array16Bit (Vector2[] arr, BinaryWriter buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        // Calculate bounding box of the array
        Vector2 bmin = arr[0] - new Vector2 (0.001f, 0.001f);
        Vector2 bmax = arr[0] + new Vector2 (0.001f, 0.001f);
        for (var i = 0; i < length; ++i)
        {
            var v = arr[i];

            bmin.x = Mathf.Min (bmin.x, v.x);
            bmin.y = Mathf.Min (bmin.y, v.y);
            bmax.x = Mathf.Max (bmax.x, v.x);
            bmax.y = Mathf.Max (bmax.y, v.y);
        }

        // Write bounds to stream
        buf.Write (bmin.x);
        buf.Write (bmax.x);
        buf.Write (bmin.y);
        buf.Write (bmax.y);

        // Encode vectors as 16 bit integer components between the bounds
        //foreach (var v in arr)
        for (var i = 0; i < length; ++i)
        {
            var v = arr[i];
            var xx = (v.x - bmin.x) / (bmax.x - bmin.x) * 65535.0f;
            var yy = (v.y - bmin.y) / (bmax.y - bmin.y) * 65535.0f;
            var ix = (ushort)xx;
            var iy = (ushort)yy;
            buf.Write (ix);
            buf.Write (iy);
        }
    }

    public static void ReadVector2Array32Bit (Vector2[] arr, BinaryReader buf)
    {
        if (arr.Length == 0)
            return;

        for (var i = 0; i < arr.Length; ++i)
        {
            float x = buf.ReadSingle ();
            float y = buf.ReadSingle ();
            arr[i] = new Vector2 (x, y);
        }
    }

    public static void WriteVector2Array32Bit (Vector2[] arr, BinaryWriter buf)
    {
        if (arr.Length == 0)
            return;

        foreach (var v in arr)
        {
            buf.Write (v.x);
            buf.Write (v.y);
        }
    }

    public static void ReadVector3ArrayBytes (Vector3[] arr, BinaryReader buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        // decode vectors as 8 bit integers components in -1.0f .. 1.0f range
        for (var i = 0; i < length; ++i)
        {
            byte ix = buf.ReadByte ();
            byte iy = buf.ReadByte ();
            byte iz = buf.ReadByte ();
            float xx = (ix - 128.0f) / 127.0f;
            float yy = (iy - 128.0f) / 127.0f;
            float zz = (iz - 128.0f) / 127.0f;
            arr[i] = new Vector3 (xx, yy, zz);
        }
    }

    public static void WriteVector3ArrayBytes (Vector3[] arr, BinaryWriter buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        // encode vectors as 8 bit integers components in -1.0f .. 1.0f range
        for (var i = 0; i < length; ++i)
        {
            var v = arr[i];
            var ix = (byte)Mathf.Clamp (v.x * 127.0f + 128.0f, 0.0f, 255.0f);
            var iy = (byte)Mathf.Clamp (v.y * 127.0f + 128.0f, 0.0f, 255.0f);
            var iz = (byte)Mathf.Clamp (v.z * 127.0f + 128.0f, 0.0f, 255.0f);
            buf.Write (ix);
            buf.Write (iy);
            buf.Write (iz);
        }
    }

    public static void ReadVector4ArrayBytes (Vector4[] arr, BinaryReader buf)
    {
        if (arr.Length == 0)
            return;

        // Decode vectors as 8 bit integers components in -1.0f .. 1.0f range
        for (var i = 0; i < arr.Length; ++i)
        {
            byte ix = buf.ReadByte ();
            byte iy = buf.ReadByte ();
            byte iz = buf.ReadByte ();
            byte iw = buf.ReadByte ();
            float xx = (ix - 128.0f) / 127.0f;
            float yy = (iy - 128.0f) / 127.0f;
            float zz = (iz - 128.0f) / 127.0f;
            float ww = (iw - 128.0f) / 127.0f;
            arr[i] = new Vector4 (xx, yy, zz, ww);
        }
    }

    public static void WriteVector4ArrayBytes (Vector4[] arr, BinaryWriter buf)
    {
        if (arr.Length == 0)
            return;

        // Encode vectors as 8 bit integers components in -1.0f .. 1.0f range
        foreach (var v in arr)
        {
            var ix = (byte)Mathf.Clamp (v.x * 127.0f + 128.0f, 0.0f, 255.0f);
            var iy = (byte)Mathf.Clamp (v.y * 127.0f + 128.0f, 0.0f, 255.0f);
            var iz = (byte)Mathf.Clamp (v.z * 127.0f + 128.0f, 0.0f, 255.0f);
            var iw = (byte)Mathf.Clamp (v.w * 127.0f + 128.0f, 0.0f, 255.0f);
            buf.Write (ix);
            buf.Write (iy);
            buf.Write (iz);
            buf.Write (iw);
        }
    }

    public static void ReadColorArray16Bit (Color[] arr, BinaryReader buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        Vector3[] array = new Vector3[arr.Length];
        ReadVector3Array16Bit (array, buf);

        for (var i = 0; i < length; ++i)
        {
            var v = array[i];
            arr[i] = new Color (v.x, v.y, v.z, 1.0f);
        }
    }

    public static void WriteColorArray16Bit (Color[] arr, BinaryWriter buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        Vector3[] array = new Vector3[arr.Length];
        for (var i = 0; i < length; i++)
        {
            var color = arr[i];
            array[i] = new Vector3 (color.r, color.g, color.b);
        }

        WriteVector3Array16Bit (array, buf);
    }

    public static void ReadColor32Array16Bit (Color32[] arr, BinaryReader buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        for (var i = 0; i < length; ++i)
        {
            var r = buf.ReadByte ();
            var g = buf.ReadByte ();
            var b = buf.ReadByte ();
            arr[i] = new Color32 (r, g, b, 255);
        }
    }

    public static void WriteColor32Array16Bit (Color32[] arr, BinaryWriter buf)
    {
        var length = arr.Length;
        if (length == 0)
            return;

        for (var i = 0; i < length; ++i)
        {
            var color = arr[i];
            buf.Write (color.r);
            buf.Write (color.g);
            buf.Write (color.b);
        }
    }

    public static void ReadColorArray32Bit (Color[] arr, BinaryReader buf)
    {
        if (arr.Length == 0)
            return;

        for (var i = 0; i < arr.Length; ++i)
        {
            var r = buf.ReadSingle ();
            var g = buf.ReadSingle ();
            var b = buf.ReadSingle ();
            arr[i] = new Color (r, g, b, 1.0f);
        }
    }

    public static void WriteColorArray32Bit (Color[] arr, BinaryWriter buf)
    {
        if (arr.Length == 0)
            return;

        foreach (var color in arr)
        {
            buf.Write (color.r);
            buf.Write (color.g);
            buf.Write (color.b);
        }
    }
    #endregion
}