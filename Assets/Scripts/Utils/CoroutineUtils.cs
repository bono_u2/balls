﻿using System.Collections;
using System.Threading;
using System;
using UnityEngine;

public static class CoroutineUtils
{
    #region Public static methods
    public static IEnumerator Then (this IEnumerator _enumerator, Action _action)
    {
        yield return _enumerator;

        _action ();
    }
    #endregion
}

public class DoInBackground : CustomYieldInstruction
{
    #region Vars
    private Thread thread;
    private Action task;

    private bool isFinished;
    #endregion

    public DoInBackground (Action _task)
    {
        task = _task;

        thread = new Thread (Work);
        thread.Name = "backgroundThread" + thread.ManagedThreadId;
        thread.IsBackground = true;
        thread.Start (null);
    }

    #region Get / Set
    public override bool keepWaiting
    {
        get { return !isFinished; }
    }
    #endregion

    #region Private methods
    private void Work (object _args)
    {
        task.Invoke ();

        isFinished = true;
    }
    #endregion
}