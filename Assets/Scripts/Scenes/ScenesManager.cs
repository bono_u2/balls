﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ScenesManager : Singleton<ScenesManager>
{
    #region Vars

    #endregion

    #region Public static methods
    public static bool IsSceneLoaded (string _toLoad)
    {
        return SceneManager.GetSceneByName (_toLoad).isLoaded;
    }
    #endregion

    #region Public static coroutines
    public static IEnumerator LoadSceneAsync (string _toLoad, bool _additive = true)
    {
        if (string.IsNullOrEmpty (_toLoad))
            yield break;

        if (IsSceneLoaded (_toLoad))
            yield break;

        yield return UnloadScenesAsync ();

        var loading = SceneManager.LoadSceneAsync (_toLoad, _additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
        while (!loading.isDone)
        {
            yield return null;
        }

        var loaded = SceneManager.GetSceneByName (_toLoad);
        if (loaded.IsValid ())
            SceneManager.SetActiveScene (loaded);
    }

    public static IEnumerator UnloadScenesAsync ()
    {
        // удаляем все кроеме главной сцены асинхронно
        for (int i = 1; i < SceneManager.sceneCount; i++)
        {
            var scene = SceneManager.GetSceneAt (i);
            SceneManager.UnloadSceneAsync (scene);
        }

        // чтобы не ждать удаления каждой из сцен последовательно, ждем здесь удаления их всех сразу
        yield return new WaitWhile (() => SceneManager.sceneCount > 1);
    }
    #endregion
}