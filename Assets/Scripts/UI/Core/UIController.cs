﻿using System.Collections.Generic;
using System;
using UnityEngine;

public abstract class UIController<T> : MonoBehaviour where T : UIEvent
{
    #region Vars
    private Dictionary<Enum, UIEventWrapper<T>> wrappers = new Dictionary<Enum, UIEventWrapper<T>> ();
    #endregion

    #region Public methods
    public abstract void Close ();

    /******************** Events ********************/

    public void AddListener (Enum _eventType, UIEventWrapper<T>.UIEventHandler _listener)
    {
        UIEventWrapper<T> eventWrapper = null;
        if (!wrappers.TryGetValue (_eventType, out eventWrapper))
        {
            eventWrapper = new UIEventWrapper<T> ();
            eventWrapper.OnHandler += _listener;
            wrappers.Add (_eventType, eventWrapper);
        }
        else
            eventWrapper.OnHandler += _listener;
    }

    public void RemoveListener (Enum _eventType, UIEventWrapper<T>.UIEventHandler _listener)
    {
        UIEventWrapper<T> eventWrapper = null;
        if (wrappers.TryGetValue (_eventType, out eventWrapper))
            eventWrapper.OnHandler -= _listener;
    }

    public void Dispatch (Enum _eventType, object _data = null)
    {
        T uiEvent = (T)Activator.CreateInstance (typeof (T), this, _eventType, _data);
        UIEventWrapper<T> eventWrapper = null;
        if (wrappers.TryGetValue (_eventType, out eventWrapper))
            eventWrapper.Invoke (uiEvent);
    }
    #endregion
}

public class UIEventWrapper<T> where T : UIEvent
{
    public delegate void UIEventHandler (T _event);

    #region Vars
    public event UIEventHandler OnHandler;
    #endregion

    #region Public methods
    public void Invoke (T _event)
    {
        if (OnHandler != null)
            OnHandler (_event);
    }
    #endregion
}