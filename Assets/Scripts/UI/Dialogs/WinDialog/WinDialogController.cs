﻿using System;
using UnityEngine;

public class WinDialogController : DialogController
{
    #region Public methods
    public void OnCloseBtnHandler ()
    {
        Close ();
    }
    #endregion
}