﻿using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class ErrorDialogController : DialogController
{
    #region Vars
    // reserved
    #endregion

    #region Public methods
    public void OnCopyStackBtnHandler ()
    {

    }

    public void OnCloseBtnHandler ()
    {
        Close ();
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        
    }

    protected override void Destroy ()
    {
        
    }
    #endregion
}