﻿using System;
using UnityEngine;

public class SettingsDialogController : DialogController
{
    #region Vars
    // reserved
    #endregion

    #region Public methods
    public void OnErrorBtnHandler ()
    {
        throw new Exception ("Error!");
    }

    public void OnCloseBtnHandler ()
    {
        Close ();
    }
    #endregion
}