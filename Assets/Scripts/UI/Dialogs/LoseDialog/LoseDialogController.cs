﻿using System;
using UnityEngine;

public class LoseDialogController : DialogController
{
    #region Public methods
    public void OnCloseBtnHandler ()
    {
        Close ();
    }
    #endregion
}