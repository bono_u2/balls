﻿using System;

public class DialogEvent : UIEvent
{
    public DialogEvent (object _target, Enum _type, object _data = null) : base (_target, _type, _data) { }

    #region Get / Set
    public new DialogController Target
    {
        get { return (DialogController)base.Target; }
    }

    public new DialogEventType Type
    {
        get { return (DialogEventType)base.Type; }
    }
    #endregion
}