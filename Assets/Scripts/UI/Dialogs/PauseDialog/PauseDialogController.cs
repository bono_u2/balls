﻿using System;
using UnityEngine;

public class PauseDialogController : DialogController
{
    #region Public methods
    public void OnRestartBtnHandler ()
    {
        Result = 0;

        Close ();
    }

    public void OnExitLevelBtnHandler ()
    {
        Result = 1;

        Close ();
    }

    public void OnResumeBtnHandler ()
    {
        Close ();
    }
    #endregion
}