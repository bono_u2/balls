﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class MainMenuScreenController : ScreenController
{
    #region Public methods
    public void OnPlayBtnHandler ()
    {
        StartCoroutine (Play ());
    }

    public void OnSettingsBtnHandler ()
    {
        StartCoroutine (Settings ());
    }

    public void OnQuitBtnHandler ()
    {
        StartCoroutine (Quit ());
    }
    #endregion

    #region Protected methods
    protected override void Init ()
    {
        var level = GlobalData.Config.Levels.First ();

        Console.Info (string.Format ("Id: {0}, Scene: {1}", level.Id, level.Scene));
    }
    #endregion

    #region Coroutines
    private IEnumerator Play ()
    {
        // создаем и показываем сплэшскрин для загрузки уровня
        var splashScreenController = SplashScreensManager.CreateSplashScreen<LoadLevelSplashScreenController> (SplashScreens.LOAD_LEVEL_SPLASH_SCREEN_NAME);
        yield return splashScreenController.WaitShowing ();

        // скрываем текущий скрин
        Hide ();

        // создаем и показываем скрин HUD для игры на уровне
        ScreensManager.CreateScreen<HUDScreenController> (Screens.HUD_SCREEN_NAME);

        // загружаем игровую сцену
        yield return ScenesManager.LoadSceneAsync ("Level1", true);

        // показываем предигровой диалог
        DialogsManager.CreateDialog<StartLevelDialogController> (Dialogs.START_LEVEL_DIALOG_NAME);

        // ждем пол секундя для уверенности что все прогрузилост
        yield return new WaitForSeconds (0.5f);

        // закрываем сплэшскрин
        splashScreenController.Close ();
        yield return splashScreenController.WaitHiding ();

        // закрываем текущий скрин
        Close ();
    }

    private IEnumerator Settings ()
    {
        // создаем и показываем диалог настроек
        var dialogController = DialogsManager.CreateDialog<SettingsDialogController> (Dialogs.SETTINGS_DIALOG_NAME);
        yield return dialogController.Wait ();
    }

    private IEnumerator Quit ()
    {
        // создаем и показываем диалог подтверждения выхода из игры. После этого ждем закрытия диалога и результата
        var dialogController = DialogsManager.CreateDialog<QuitDialogController> (Dialogs.QUIT_DIALOG_NAME);
        yield return dialogController.Wait ();

        // проверяем результат. если результат не null то значит выход из игры подтвержден
        if (dialogController.Result != null)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit ();
#endif
        }
    }
    #endregion
}