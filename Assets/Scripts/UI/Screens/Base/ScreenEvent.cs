﻿using System;

public class ScreenEvent : UIEvent
{
    public ScreenEvent (object _target, Enum _type, object _data = null) : base (_target, _type, _data) { }

    #region Get / Set
    public new ScreenController Target
    {
        get { return (ScreenController)base.Target; }
    }

    public new ScreenEventType Type
    {
        get { return (ScreenEventType)base.Type; }
    }
    #endregion
}