﻿using System.Collections;
using UnityEngine;

public class HUDScreenController : ScreenController
{
    #region Unity methods
    void Start ()
    {
        EventManager.AddEventListener (GamePhaseChangedEvent.ID, OnGamePhaseChangedHandler);
    }

    void OnDestroy ()
    {
        EventManager.RemoveEventListener (GamePhaseChangedEvent.ID, OnGamePhaseChangedHandler);
    }
    #endregion

    #region Public methods
    public void OnPauseBtnHandler ()
    {
        StartCoroutine (Pause ());
    }
    #endregion

    #region Private methods
    private void OnGamePhaseChangedHandler (FreeTeam.Events.Event _event)
    {
        var e = (GamePhaseChangedEvent)_event;
        Console.Info (string.Format ("GamePhase: {0}", e.Phase.ToString ()));

        if (e.Phase == GamePhase.Win)
            StartCoroutine (Win ());
        else if (e.Phase == GamePhase.Lose)
            StartCoroutine (Lose ());
    }
    #endregion

    #region Coroutines
    private IEnumerator Pause ()
    {
        // скрываем скрин
        Hide ();

        // сохраняем текущую фазу
        var phase = GameController.Instance.Phase;

        // назначаем новую фазу в виде паузы
        GameController.Instance.Phase = GamePhase.Pause;

        // создаем и показываем диалог паузы. ждем результата
        var pauseDialogController = DialogsManager.CreateDialog<PauseDialogController> (Dialogs.PAUSE_DIALOG_NAME);
        yield return pauseDialogController.Wait ();

        if (pauseDialogController.Result != null)
        {
            var result = (int)pauseDialogController.Result;
            if (result == 0)            // restart level
            {
                Console.Log ("Restart level...");

                yield return Restart ();
            }
            else if (result == 1)       // exit to main menu
            {
                Console.Log ("Exit level...");

                yield return ExitToMainMenu ();
            }
        }
        else
            // возвращаем игровую фазу
            GameController.Instance.Phase = phase;

        // показываем скрин
        Show ();
    }

    private IEnumerator Win ()
    {
        yield return new WaitForSeconds (1.0f);

        var winDialogController = DialogsManager.CreateDialog<WinDialogController> (Dialogs.WIN_DIALOG_NAME);
        yield return winDialogController.Wait ();

        yield return Restart ();
    }

    private IEnumerator Lose ()
    {
        yield return new WaitForSeconds (1.0f);

        var loseDialogController = DialogsManager.CreateDialog<LoseDialogController> (Dialogs.LOSE_DIALOG_NAME);
        yield return loseDialogController.Wait ();

        yield return Restart ();
    }

    private IEnumerator Restart ()
    {
        // создаем и показываем сплэшскрин для загрузки уровня
        var splashScreenController = SplashScreensManager.CreateSplashScreen<LoadLevelSplashScreenController> (SplashScreens.LOAD_LEVEL_SPLASH_SCREEN_NAME);
        yield return splashScreenController.WaitShowing ();

        // выгружаем все сцены кроме главной
        yield return ScenesManager.UnloadScenesAsync ();

        // загружаем игровую сцену
        yield return ScenesManager.LoadSceneAsync ("Level1", true);

        // показываем предигровой диалог
        DialogsManager.CreateDialog<StartLevelDialogController> (Dialogs.START_LEVEL_DIALOG_NAME);

        // ждем пол секундя для уверенности что все прогрузилост
        yield return new WaitForSeconds (0.5f);

        // закрываем сплэшскрин
        splashScreenController.Close ();
        yield return splashScreenController.WaitHiding ();
    }

    private IEnumerator ExitToMainMenu ()
    {
        // создаем и показываем сплэшскрин для загрузки игры
        var splashScreenController = SplashScreensManager.CreateSplashScreen<LoadGameSplashScreenController> (SplashScreens.LOAD_GAME_SPLASH_SCREEN_NAME);
        yield return splashScreenController.WaitShowing ();

        // скрываем текущий скрин
        Hide ();

        // выгружаем все сцены кроме главной
        yield return ScenesManager.UnloadScenesAsync ();

        // создаем и показываем скрин главного меню игры
        ScreensManager.CreateScreen<MainMenuScreenController> (Screens.MAIN_MENU_SCREEN_NAME);

        // ждем пол секундя для уверенности что все прогрузилост
        yield return new WaitForSeconds (0.5f);

        // закрываем сплэшскрин
        splashScreenController.Close ();
        yield return splashScreenController.WaitHiding ();

        // закрываем текущий скрин
        Close ();
    }
    #endregion
}