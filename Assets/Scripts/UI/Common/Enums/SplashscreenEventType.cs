﻿public enum SplashScreenEventType
{
    Init,
    Close,
    Destruction,
    Show,
    Hide,
}