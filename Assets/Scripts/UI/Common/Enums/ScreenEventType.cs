﻿public enum ScreenEventType
{
    Init,
    Close,
    Destruction,
    Show,
    Hide,
}