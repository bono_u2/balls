﻿using System;

public class SplashScreenEvent : UIEvent
{
    public SplashScreenEvent (object _target, Enum _type, object _data = null) : base (_target, _type, _data) { }

    #region Get / Set
    public new SplashScreenController Target
    {
        get { return (SplashScreenController)base.Target; }
    }

    public new SplashScreenEventType Type
    {
        get { return (SplashScreenEventType)base.Type; }
    }
    #endregion
}