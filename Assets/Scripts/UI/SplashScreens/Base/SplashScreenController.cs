﻿using System.Collections;
using UnityEngine;

public class SplashScreenController : UIController<SplashScreenEvent>
{
    #region Vars
    private Animation animation_;
    private CanvasGroup canvasGroup_;
    #endregion

    #region Get / Set
    public bool IsInited { get; protected set; }
    public bool IsRunning { get; private set; }
    public bool IsShowed { get; private set; }

    public object Data { get; set; }
    public object Result { get; protected set; }

    protected Animation Animation
    {
        get
        {
            if (!animation_)
            {
                animation_ = this.gameObject.GetComponent<Animation> ();
                if (!animation_)
                    animation_ = this.gameObject.AddComponent<Animation> ();
            }

            return animation_;
        }
    }
    protected CanvasGroup CanvasGroup
    {
        get
        {
            if (!canvasGroup_)
            {
                canvasGroup_ = this.gameObject.GetComponent<CanvasGroup> ();
                if (!canvasGroup_)
                    canvasGroup_ = this.gameObject.AddComponent<CanvasGroup> ();
            }

            return canvasGroup_;
        }
    }
    #endregion

    #region Public methods
    public sealed override void Close ()
    {
        Dispatch (SplashScreenEventType.Close);

        StartCoroutine (Closing ());
    }

    public void Show ()
    {
        StartCoroutine (Showing ());
    }

    public void Hide ()
    {
        StartCoroutine (Hiding ());
    }
    #endregion

    #region Protected methods
    protected virtual void Init ()
    {
    }

    protected virtual void Destroy ()
    {
    }
    #endregion

    #region Private methods
    private void Initialization ()
    {
        if (IsInited)
            return;

        Init ();

        IsInited = true;
        IsRunning = true;

        Dispatch (SplashScreenEventType.Init);

        Show ();
    }

    private void Destruction ()
    {
        Dispatch (SplashScreenEventType.Destruction);

        IsRunning = false;
    }
    #endregion

    #region Coroutines
    private IEnumerator Showing ()
    {
        CanvasGroup.alpha = 1.0f;
        CanvasGroup.interactable = true;
        CanvasGroup.blocksRaycasts = true;

        if (Animation && !IsShowed)
        {
            var clip = Animation.GetClip ("ShowSplashScreenAnimation");
            if (clip != null)
            {
                Animation.clip = clip;
                Animation.Play ();
                yield return WaitForAnimation (Animation);
                Animation.clip = null;
            }
        }

        IsShowed = true;

        Dispatch (SplashScreenEventType.Show);
    }

    private IEnumerator Hiding ()
    {
        if (Animation && IsShowed)
        {
            var clip = Animation.GetClip ("HideSplashScreenAnimation");
            if (clip != null)
            {
                Animation.clip = clip;
                Animation.Play ();
                yield return WaitForAnimation (Animation);
                Animation.clip = null;
            }
        }

        IsShowed = false;

        CanvasGroup.alpha = 0.0f;
        CanvasGroup.interactable = false;
        CanvasGroup.blocksRaycasts = false;

        Dispatch (SplashScreenEventType.Hide);
    }

    private IEnumerator Closing ()
    {
        yield return Hiding ();

        Destruction ();
    }

    private IEnumerator WaitForAnimation (Animation _animation)
    {
        do
        {
            yield return null;
        } while (_animation.isPlaying);
    }

    public IEnumerator WaitShowing ()
    {
        while (!IsShowed)
            yield return null;
    }

    public IEnumerator WaitHiding ()
    {
        while (IsShowed)
            yield return null;
    }

    public IEnumerator Wait ()
    {
        while (IsRunning)
            yield return null;
    }
    #endregion
}