﻿public class SplashScreens
{
    #region Constants
    public const string LOAD_GAME_SPLASH_SCREEN_NAME = "LoadGameSplashScreen/LoadGameSplashScreen";
    public const string LOAD_LEVEL_SPLASH_SCREEN_NAME = "LoadLevelSplashScreen/LoadLevelSplashScreen";
    #endregion
}