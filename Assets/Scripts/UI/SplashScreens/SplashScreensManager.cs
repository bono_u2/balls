﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SplashScreensManager : MonoBehaviour
{
    #region Instance
    private static SplashScreensManager instance_;
    public static SplashScreensManager Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<SplashScreensManager> ();

            if (instance_ == null)
                Console.Error (string.Format ("No <b>{0}</b> instance set", typeof (SplashScreensManager).FullName));

            return instance_;
        }
    }
    #endregion

    #region Static vars
    private const string SPLASH_SCREEN_PREFABS_PATH = "Ui/SplashScreens/{0}";
    #endregion

    #region Vars
    private List<SplashScreenController> splashScreens = new List<SplashScreenController> ();
    #endregion

    #region Unity methods
    void Awake ()
    {
        UiRaycaster = GetComponentInParent<UnityEngine.UI.GraphicRaycaster> ();
    }
    #endregion

    #region Get / Set
    public UnityEngine.UI.GraphicRaycaster UiRaycaster { get; private set; }

    public SplashScreenController Current { get { return splashScreens.LastOrDefault (); } }
    #endregion

    #region Public methods
    public SplashScreenController Create (string _splashScreenName, object _data = null, bool _keepHistory = true)
    {
        return Create<SplashScreenController> (_splashScreenName, _data, _keepHistory);
    }

    public T Create<T> (string _splashScreenName, object _data = null, bool _keepHistory = true) where T : SplashScreenController
    {
        // check dialog name
        if (string.IsNullOrEmpty (_splashScreenName))
        {
            // Debug
            Console.Error ("<color=#eeeeee>(SplashScreensManager) SplashScreenName is null or empty!</color>", ConsoleChannelType.UI);

            return null;
        }

        // Prefab path
        var prefabPath = string.Format (SPLASH_SCREEN_PREFABS_PATH, _splashScreenName);

        // Load Prefab
        var prefab = ResourcesManager.Load<GameObject> (prefabPath);
        if (System.Object.ReferenceEquals (prefab, null))
        {
            // Debug
            Console.Error (string.Format ("<color=#eeeeee>(SplashScreensManager) Prefab \"<b>{0}</b>\" no found!</color>", _splashScreenName), ConsoleChannelType.UI);

            return null;
        }

        // Instantiate Prefab to GameObject
        var splashScreenGO = Instantiate (prefab, transform, false);

        // Get DialogController component
        var splashScreenController = splashScreenGO.GetComponent<T> ();
        if (!splashScreenController)
        {
            // Debug
            Console.Error (string.Format ("<color=#eeeeee>(SplashScreensManager) Controller \"<b>{0}</b>\" in prefab \"<b>{1}</b>\" no found!</color>", typeof (T), _splashScreenName), ConsoleChannelType.UI);

            // Destroy screen instance
            splashScreenGO.SafeDestroy ();
            return null;
        }

        // Add DialogController to dialogs list
        splashScreens.Add (splashScreenController);

        // set dialog Data
        splashScreenController.Data = _data;

        // подписываемся на событие закрытия диалога
        splashScreenController.AddListener (SplashScreenEventType.Destruction, OnDestructionSplashScreenHandler);

        // Init DialogController
        splashScreenController.SendMessage ("Initialization");

        // Debug
        Console.Info (string.Format ("<color=#eeeeee>(SplashScreensManager) SplashScreens count: <b>{0}</b></color>", splashScreens.Count), ConsoleChannelType.UI);

        return splashScreenController;
    }

    public void Destroy ()
    {
        // Last DialogController
        var splashScreenController = splashScreens.LastOrDefault ();

        // Destroy splashScreen
        Destroy (splashScreenController);
    }

    public void Destroy (SplashScreenController _splashScreenController)
    {
        // Check DialogController
        if (!_splashScreenController)
        {
            Console.Error ("(SplashScreensManager) Controller is null!", ConsoleChannelType.UI);
            return;
        }

        // отписываемся от события
        _splashScreenController.RemoveListener (SplashScreenEventType.Destruction, OnDestructionSplashScreenHandler);

        // Call Closing SplashScreenController
        _splashScreenController.SendMessage ("Destroy");

        // Remove SplashScreenController from dialogs list
        splashScreens.Remove (_splashScreenController);

        // Destroy SplashScreen GameObject
        GameObject.Destroy (_splashScreenController.gameObject);

        // Debug
        Console.Info (string.Format ("<color=#eeeeee>(SplashScreensManager) Screens count: <b>{0}</b></color>", splashScreens.Count), ConsoleChannelType.UI);
    }

    public void DestroyAll ()
    {
        for (var i = 0; i < splashScreens.Count; i++)
            Destroy (splashScreens[i]);
    }
    #endregion

    #region Private methods
    private void OnDestructionSplashScreenHandler (SplashScreenEvent _event)
    {
        // уничтожаем диалог
        Destroy (_event.Target);
    }
    #endregion

    #region Public static methods
    public static SplashScreenController CreateSplashScreen (string _screenName, object _data = null)
    {
        return SplashScreensManager.Instance.Create (_screenName, _data);
    }

    public static T CreateSplashScreen<T> (string _screenName, object _data = null) where T : SplashScreenController
    {
        return SplashScreensManager.Instance.Create<T> (_screenName, _data);
    }

    public static void DestroySplashScreen ()
    {
        SplashScreensManager.Instance.Destroy ();
    }

    public static void DestroySplashScreen (SplashScreenController _screenController)
    {
        SplashScreensManager.Instance.Destroy (_screenController);
    }

    public static void DestroyAllSplashScreeens ()
    {
        SplashScreensManager.Instance.DestroyAll ();
    }
    #endregion
}