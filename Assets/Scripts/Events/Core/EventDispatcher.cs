﻿using System.Collections.Generic;

namespace FreeTeam.Events
{
    public class EventDispatcher
    {
        #region Vars
        private Dictionary<string, EventWrapper> wrappers;
        #endregion

        public EventDispatcher ()
        {
            wrappers = new Dictionary<string, EventWrapper> ();
        }

        #region Public methods
        public void AddListener (string _eventType, EventHandler _listener)
        {
            EventWrapper eventWrapper = null;
            if (!wrappers.TryGetValue (_eventType, out eventWrapper))
            {
                eventWrapper = new EventWrapper ();
                eventWrapper.OnHandler += _listener;
                wrappers.Add (_eventType, eventWrapper);
            }
            else
                eventWrapper.OnHandler += _listener;
        }

        public void RemoveListener (string _eventType, EventHandler _listener)
        {
            EventWrapper thisEvent = null;
            if (wrappers.TryGetValue (_eventType, out thisEvent))
                thisEvent.OnHandler -= _listener;
        }

        public void Dispatch (Event _event)
        {
            EventWrapper eventWrapper = null;
            if (wrappers.TryGetValue (_event.Type, out eventWrapper))
                eventWrapper.Invoke (_event);
        }
        #endregion
    }
}