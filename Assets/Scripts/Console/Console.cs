﻿using FreeTeam.Events;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Console : EventDispatcher
{
    #region Instance
    private static Console instance_;
    private static Console Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = new Console ();

            return instance_;
        }
    }
    #endregion

    #region Vars
    private int maxLogsCount = 256;
    private List<ConsoleLog> logs = new List<ConsoleLog> ();
    #endregion

    public Console () { }

    #region Get / Set
    public List<ConsoleLog> FullLog
    {
        get { return logs; }
    }

    public int MaxLogsCount
    {
        get { return maxLogsCount; }
        set { maxLogsCount = value; }
    }
    #endregion

    #region Public methods 
    public void Add (ConsoleLog _consoleLog)
    {
        if (logs.Count >= maxLogsCount)
            logs.RemoveAt (0);

        logs.Add (_consoleLog);

        Dispatch (new ConsoleLogEvent (_consoleLog));
    }

    public void Clear ()
    {
        logs.Clear ();
    }
    #endregion

    #region Public static methods
    public static void Log (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log (string.Format ("<color=#eeeeee><b>[Log]:</b></color> {0}", text));
#endif

        Console.Instance.Add (new ConsoleLog (ConsoleLogType.Log, text, _channel));
    }

    public static void Message (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log (string.Format ("<color=#99BB33><b>[Message]:</b></color> {0}", text));
#endif

        Console.Instance.Add (new ConsoleLog (ConsoleLogType.Message, text, _channel));
    }

    public static void Info (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log (string.Format ("<color=#5998ff><b>[Info]:</b></color> {0}", text));
#endif

        Console.Instance.Add (new ConsoleLog (ConsoleLogType.Info, text, _channel));
    }

    public static void Warning (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log (string.Format ("<color=#ffcb49><b>[Warning]:</b></color> {0}", text));
#endif

        Console.Instance.Add (new ConsoleLog (ConsoleLogType.Warning, text, _channel));
    }

    public static void Error (string _text, ConsoleChannelType _channel = ConsoleChannelType.Default)
    {
        StringBuilder sb = new StringBuilder (_text);
        var text = sb.ToString (0, Mathf.Min (16000, _text.Length));

#if UNITY_EDITOR
        Debug.Log (string.Format ("<color=#b50707><b>[Error]:</b></color> {0}", text));
#endif

        Console.Instance.Add (new ConsoleLog (ConsoleLogType.Error, text, _channel));
    }

    public static List<ConsoleLog> GetFullConsoleLog ()
    {
        return Instance.FullLog;
    }

    public static void ClearLog ()
    {
        Instance.Clear ();
    }
    #endregion
}