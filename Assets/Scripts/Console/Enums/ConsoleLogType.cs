﻿using System;

[Flags]
public enum ConsoleLogType
{
    Log = 1,
    Info = 2,
    Message = 4,
    Warning = 8,
    Error = 16,
}