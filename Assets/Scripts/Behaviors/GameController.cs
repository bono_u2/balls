﻿using System.Collections;
using System;
using UnityEngine;

public class GameController : Singleton<GameController>
{
    #region Vars
    private GamePhase phase = GamePhase.Ready;
    #endregion

    #region Unity methods
    void Awake ()
    {
        Phase = GamePhase.Ready;
    }
    #endregion

    #region Get / Set
    public GamePhase Phase
    {
        get { return phase; }
        set
        {
            phase = value;

            EventManager.DispatchEvent (new GamePhaseChangedEvent (phase));
        }
    }
    #endregion

    #region Public methods
    // reserved
    #endregion
}