﻿using FreeTeam.Events;
using UnityEngine;

public class EnemyController : BallController
{
    #region Unity methods
    void FixedUpdate ()
    {
        if (!rb)
            return;

        var q = Quaternion.Euler (gameCameraController.RotationAngles + new Vector3 (0.0f, gameCameraController.offsetRotation.y, 0.0f));
        var v = new Vector3 (0.0f, -9.8f, 0.0f) * rb.mass;

        rb.AddForce (q * v);
    }
    #endregion

    #region Protected methods
    protected override void OnGamePhaseChangedHandler (FreeTeam.Events.Event _event)
    {
        var e = (GamePhaseChangedEvent)_event;

        switch (e.Phase)
        {
            case GamePhase.Play:
                rb.isKinematic = false;
                rb.velocity = velocity;
                break;
            case GamePhase.Pause:
                velocity = rb.velocity;
                rb.isKinematic = true;
                break;
            case GamePhase.Ready:
            case GamePhase.Win:
            case GamePhase.Lose:
                rb.isKinematic = true;
                break;
        }
    }
    #endregion
}