﻿using UnityEngine;

public abstract class BallController : MonoBehaviour
{
    #region Vars
    protected Vector3 velocity;

    protected Rigidbody rb;
    protected GameController gameController;
    protected GameCameraController gameCameraController;

    protected Material material;
    #endregion

    #region Unity methods
    void Start ()
    {
        rb = GetComponent<Rigidbody> ();

        material = GetComponent<MeshRenderer> ().material;

        gameController = GameController.Instance;
        gameCameraController = GameCameraController.Instance;

        EventManager.AddEventListener (GamePhaseChangedEvent.ID, OnGamePhaseChangedHandler);
    }

    void OnDestroy ()
    {
        EventManager.RemoveEventListener (GamePhaseChangedEvent.ID, OnGamePhaseChangedHandler);
    }
    #endregion

    #region Protected methods
    protected virtual void OnGamePhaseChangedHandler (FreeTeam.Events.Event _event)
    {
    }
    #endregion
}