﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

public class PlayerController : BallController
{
    #region Vars
    private Dictionary<string, System.Action> collisionActions;
    #endregion

    #region Unity methods
    void Awake ()
    {
        collisionActions = new Dictionary<string, System.Action> () {
            { "Enemy", EnemyCollision },
            { "WinTrigger", WinTriggerCollision },
        };
    }

    void FixedUpdate ()
    {
        if (gameController.Phase != GamePhase.Play)
            return;

        if (!rb)
            return;

        var q = Quaternion.Euler (gameCameraController.RotationAngles + new Vector3 (0.0f, gameCameraController.offsetRotation.y, 0.0f));
        var v = new Vector3 (0.0f, -9.8f, 0.0f) * rb.mass * 2.0f;

        rb.AddForce (q * v);
    }

    void OnCollisionEnter (Collision _collision)
    {
        Action action;
        if (collisionActions.TryGetValue (_collision.gameObject.tag, out action))
            action.Invoke ();
    }
    #endregion

    #region Protected methods
    protected override void OnGamePhaseChangedHandler (FreeTeam.Events.Event _event)
    {
        var e = (GamePhaseChangedEvent)_event;

        switch (e.Phase)
        {
            case GamePhase.Ready:
                rb.isKinematic = true;
                break;
            case GamePhase.Play:
                rb.isKinematic = false;
                rb.velocity = velocity;
                break;
            case GamePhase.Pause:
                velocity = rb.velocity;
                rb.isKinematic = true;
                break;
            case GamePhase.Win:
            case GamePhase.Lose:
                rb.isKinematic = true;
                StartCoroutine (Hide ());
                break;
        }
    }
    #endregion

    #region Private methods
    private void EnemyCollision ()
    {
        gameController.Phase = GamePhase.Lose;
        Console.Log ("<color=#FF2222><b>EnemyCollision</b></color>");
    }

    private void WinTriggerCollision ()
    {
        gameController.Phase = GamePhase.Win;
        Console.Log ("<color=#22FF22><b>WinTriggerCollision</b></color>");
    }
    #endregion

    #region Coroutines
    private IEnumerator Hide ()
    {
        yield return null;

        float duration = 1.0f;
        float time = duration;
        while (time > 0.0f)
        {
            var alpha = time / duration;
            var c = material.color;
            material.color = new Color (c.r, c.g, c.b, alpha);

            time -= Time.deltaTime;

            yield return null;
        }
    }
    #endregion
}