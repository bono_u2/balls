﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using UnityEngine;

class GlobalData
{
    #region Static vars
    public static bool IsDebug;

    public static Config Config = null;
    #endregion

    #region Public static methods
    public static void LoadConfig (bool _forced = false)
    {
        if (Config == null || _forced)
        {
            Console.Info ("Load config from <color=#228822><b>RESOURCES FOLDER</b></color>...", ConsoleChannelType.Resources);

            var content = Resources.Load<TextAsset> (URIs.PATH_CONFIG).text;

            try
            {
                Config = Config.Load (content);
            }
            catch
            {
                Config = null;
            }
        }
    }
    #endregion

    #region Coroutines
    public static IEnumerator LoadConfigAsync (bool _forced = false)
    {
        if (Config == null || _forced)
        {
            Console.Info ("Load config async from <color=#228822><b>RESOURCES FOLDER</b></color>...", ConsoleChannelType.Resources);

            var content = Resources.Load<TextAsset> (URIs.PATH_CONFIG).text;

            yield return new DoInBackground (() =>
            {
                try
                {
                    Config = Config.Load (content);
                }
                catch
                {
                    Config = null;
                }
            });
        }
    }
    #endregion
}