﻿using System.Collections;
using UnityEngine;

public class AppManager : MonoBehaviour
{
    #region Vars
    private bool isInitialized;
    #endregion

    #region Unity methods
    void Start ()
    {
        // подписываемся на логирование движком
        Application.logMessageReceivedThreaded += CaptureLogThread;

        // Загружаем нужные части игры
        LoadGame ();
    }

    void OnDestroy ()
    {
        Application.logMessageReceivedThreaded -= CaptureLogThread;
    }

    void OnApplicationQuit ()
    {
        if (!isInitialized)
            return;

        // сохраняем прогресс если нужно
    }

    void OnApplicationPause (bool _pauseStatus)
    {
        Console.Log ("[AppManager] app pause " + _pauseStatus);
        if (_pauseStatus && isInitialized)
        {
            // сохраняем прогресс если нужно
        }
    }
    #endregion

    #region Private methods
    private void LoadGame ()
    {
        SplashScreensManager.DestroyAllSplashScreeens ();
        ScreensManager.DestroyAllScreeens ();
        DialogsManager.DestroyAllDialogs ();

        StartCoroutine (LoadGameRoutine ());
    }

    private void CaptureLogThread (string _condition, string _stacktrace, LogType _type)
    {
        if (_type == LogType.Warning)
        {
        }
        else if (_type == LogType.Log || _type == LogType.Assert)
        {
        }
        else if (_type == LogType.Error)
        {
            var message = "Not critical ERROR:" + _condition + "\n" + _stacktrace;
            Console.Error (message);
        }
        else
        {
            var message = "Unhandled EXCEPTION:" + _condition + "\n" + _stacktrace;
            Console.Error (message);

            var dialogController = DialogsManager.CreateDialog<ErrorDialogController> (Dialogs.ERROR_DIALOG_NAME, _condition + "\r\n" + _stacktrace);
            StartCoroutine (dialogController.Wait ().Then (LoadGame));
        }
    }
    #endregion

    #region Coroutines
    private IEnumerator LoadGameRoutine ()
    {
        // показываем заставку если она еще не показана
        var splashScreenController = (LoadGameSplashScreenController)SplashScreensManager.Instance.Current;
        if (!splashScreenController)
        {
            splashScreenController = SplashScreensManager.CreateSplashScreen<LoadGameSplashScreenController> (SplashScreens.LOAD_GAME_SPLASH_SCREEN_NAME);
            yield return splashScreenController.WaitShowing ();
        }

        // загружаем конфиг
        yield return GlobalData.LoadConfigAsync (true);

        // открываем главное меню
        ScreensManager.CreateScreen<MainMenuScreenController> (Screens.MAIN_MENU_SCREEN_NAME);

        // 
        yield return new WaitForSeconds (0.5f);

        // проставляем флаг что мы успешно инициализировались
        isInitialized = true;

        // скрываем заставку
        splashScreenController.Close ();
        yield return splashScreenController.WaitHiding ();
    }
    #endregion
}