﻿using UnityEngine;

public class GameCameraController : Singleton<GameCameraController>
{
    #region Vars
    [Header ("Target")]
    public Transform target = null;

    [Header ("Distance")]
    public float defaultDistance = 10.0f;
    public float maxDistance = 12.5f;
    public float minDistance = 10.0f;

    [Header ("Rotation")]
    public Vector3 offsetRotation = new Vector3 (22.5f, 135.0f, 0.0f);
    public Vector3 minRotationLimit = new Vector3 (-45.0f, 0.0f, -90.0f);
    public Vector3 maxRotationLimit = new Vector3 (90.0f, 360.0f, 90.0f);
    public Vector3 speedRotation = new Vector3 (20.0f, 20.0f, 20.0f);
    public float rotationDampening = 3.0f;

    [Header ("Zoom")]
    public float zoomRate = 1f;
    public float zoomDampening = 3.0f;


    private Vector3 rotationAngles = new Vector3 (0.0f, 0.0f, 0.0f);

    private float currentDistance;
    private float desiredDistance;

    private Quaternion rotation;
    private Vector3 position;
    #endregion

    #region Unity methods
    void Start ()
    {
        currentDistance = defaultDistance;
        desiredDistance = defaultDistance;
    }

    void LateUpdate ()
    {
        if (GameController.Instance.Phase != GamePhase.Play)
            return;

        if (!target)
            return;

        UpdateInput ();

        Calculate ();
    }
    #endregion

    #region Get / Set
    public Vector3 RotationAngles
    {
        get { return rotationAngles; }
    }
    #endregion

    #region Private methods
    private void UpdateInput ()
    {
        if (GUIUtility.hotControl != 0)
            return;

        #region Mouse input
#if (UNITY_EDITOR || UNITY_STANDALONE)
        // If either mouse buttons are down, let the mouse govern camera position
        if (Input.GetMouseButton (0))
        {
            Vector2 deltaPos = new Vector2 (Input.GetAxis ("Mouse X") * speedRotation.x, Input.GetAxis ("Mouse Y") * speedRotation.y);
            MoveCamera (deltaPos);
        }

        var scrollDelta = Input.GetAxis ("Mouse ScrollWheel");
        if (scrollDelta != 0)
            PinchZoom (scrollDelta);
#endif
        #endregion

        #region Touch input
#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE)
        if (Input.touchCount > 0)
        {
            if (Input.touchCount == 1)
            {
                Touch touchZero = Input.GetTouch (0);
                if (touchZero.phase == TouchPhase.Moved)
                    MoveCamera (touchZero.deltaPosition * 0.1f);
            }
            else if (Input.touchCount == 2)
            {
                DetectTouchMovement.Calculate ();
                if (Mathf.Abs (DetectTouchMovement.pinchDistanceDelta) > 0)
                    PinchZoom (DetectTouchMovement.pinchDistanceDelta * 0.5f);
            }
        }
#endif
        #endregion
    }

    private void Calculate ()
    {
        // calc base quaternions
        Quaternion quatX = Quaternion.Euler (Vector3.right * rotationAngles.x);
        Quaternion quatZ = Quaternion.Euler (Vector3.forward * rotationAngles.z);

        // calc additional quaternions
        var defaultQuatX = Quaternion.Euler (Vector3.right * offsetRotation.x);
        var invQuatX = Quaternion.Inverse (defaultQuatX);
        var defaultQuatY = Quaternion.Euler (Vector3.up * offsetRotation.y);
        var invQuatY = Quaternion.Inverse (defaultQuatY);

        // Set camera rotation with lerp
        rotation = defaultQuatY * Quaternion.Lerp (invQuatY * rotation * invQuatX, quatZ * quatX, Time.deltaTime * rotationDampening) * defaultQuatX;

        // Clamp camera distance
        desiredDistance = Mathf.Clamp (desiredDistance, minDistance, maxDistance);

        // Calculate desired camera position
        position = (target.position) - (rotation * Vector3.forward * desiredDistance);

        // For smoothing, lerp distance only if either distance wasn't corrected, or correctedDistance is more than currentDistance
        currentDistance = Mathf.Lerp (currentDistance, desiredDistance, Time.deltaTime * zoomDampening);

        // Recalculate position based on the new currentDistance
        position = (target.position) - (rotation * Vector3.forward * currentDistance);

        //Finally Set rotation and position of camera
        transform.rotation = rotation;
        transform.position = position;
    }

    private void PinchZoom (float _deltaMagnitudeDiff)
    {
        desiredDistance -= _deltaMagnitudeDiff * Time.deltaTime * zoomRate * Mathf.Abs (desiredDistance) * 100.0f;
    }

    private void MoveCamera (float _deltaX, float _deltaY)
    {
        var deltaPosition = new Vector2 (_deltaX, _deltaY);
        MoveCamera (deltaPosition);
    }

    private void MoveCamera (Vector2 _deltaPosition)
    {
        rotationAngles.x -= _deltaPosition.y;
        rotationAngles.z += _deltaPosition.x;

        rotationAngles.x = rotationAngles.x % 360f;
        rotationAngles.z = rotationAngles.z % 360f;

        rotationAngles.x = Mathf.Clamp (rotationAngles.x, minRotationLimit.x, maxRotationLimit.x);
        rotationAngles.z = Mathf.Clamp (rotationAngles.z, minRotationLimit.z, maxRotationLimit.z);
    }
    #endregion
}