﻿using System;

[Serializable]
public struct Range
{
    #region Vars
    public float min;
    public float max;
    #endregion

    #region Public methods
    public float Random ()
    {
        return UnityEngine.Random.Range (min, max);
    }

    public float Random (int _digits = 0)
    {
        var d = UnityEngine.Mathf.Pow (10, _digits);
        return UnityEngine.Random.Range ((int)(min * d), (int)(max * d) + 1) / d;
    }
    #endregion
}