﻿using UnityEngine;

#pragma warning disable 0642
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    #region Instance
    private static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<T> ();

            if ((object)_instance == null)
                Debug.LogWarning ((object)string.Format ("No {0} instance set", (object)typeof (T).FullName));

            return Singleton<T>._instance;
        }
    }
    #endregion

    #region Static Get / Set
    public static bool IsInstantiated
    {
        get { return (object)Singleton<T>._instance != null; }
    }
    #endregion

    #region Unity methods
    void Awake ()
    {
        if (!Instance) ;
    }

    void OnDestroy ()
    {
        _instance = null;
    }
    #endregion
}