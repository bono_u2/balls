﻿public class GamePhaseChangedEvent : FreeTeam.Events.Event
{
    #region Constants
    public const string ID = "GamePhaseChanged";
    #endregion

    public GamePhaseChangedEvent (GamePhase _phase) : base (ID)
    {
        Phase = _phase;
    }

    #region Get / Set
    public GamePhase Phase { get; private set; }
    #endregion
}