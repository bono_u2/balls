﻿using System.Collections.Generic;
using UnityEngine;

public class EventMonoBehaviour : MonoBehaviour
{
    #region Vars
    private Dictionary<string, FreeTeam.Events.EventWrapper> wrappers = new Dictionary<string, FreeTeam.Events.EventWrapper> ();
    #endregion

    #region Public methods
    public void AddListener (string _eventType, FreeTeam.Events.EventHandler _listener)
    {
        FreeTeam.Events.EventWrapper eventWrapper = null;
        if (!wrappers.TryGetValue (_eventType, out eventWrapper))
        {
            eventWrapper = new FreeTeam.Events.EventWrapper ();
            eventWrapper.OnHandler += _listener;
            wrappers.Add (_eventType, eventWrapper);
        }
        else
            eventWrapper.OnHandler += _listener;
    }

    public void RemoveListener (string _eventType, FreeTeam.Events.EventHandler _listener)
    {
        FreeTeam.Events.EventWrapper thisEvent = null;
        if (wrappers.TryGetValue (_eventType, out thisEvent))
            thisEvent.OnHandler -= _listener;
    }

    public void Dispatch (FreeTeam.Events.Event _event)
    {
        FreeTeam.Events.EventWrapper eventWrapper = null;
        if (wrappers.TryGetValue (_event.Type, out eventWrapper))
            eventWrapper.Invoke (_event);
    }
    #endregion
}