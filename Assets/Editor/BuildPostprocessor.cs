﻿using System;
using UnityEditor.Callbacks;
using UnityEditor;
using UnityPlayerSettings = UnityEditor.PlayerSettings;

public class BuildPostprocessor
{
    #region Public static methods
    /// <summary>
    /// A normal post process method which is executed by Unity
    /// </summary>
    [PostProcessBuild]
    public static void OnPostprocessBuild (BuildTarget buildTarget, string path)
    {
        Console.Log ("[UCB] OnPostprocessBuild");
#if !UNITY_CLOUD_BUILD
        ProcessPostBuild (buildTarget, path);
#endif
        UpdateBundleVersion (buildTarget);
    }
    #endregion

    #region Private static methods
    private static void ProcessPostBuild (BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS)
        {

        }
        else if (buildTarget == BuildTarget.Android)
        {

        }
    }

    private static void UpdateBundleVersion (BuildTarget buildTarget)
    {
        string currentVersion = UnityPlayerSettings.bundleVersion;
        string[] currentVersionSplit = currentVersion.Split ('.');
        if (currentVersionSplit.Length < 3)
            currentVersionSplit = new string[] { currentVersionSplit[0], currentVersionSplit[1], "0" };

        try
        {
            int major = Convert.ToInt32 (currentVersionSplit[0]);
            int minor = Convert.ToInt32 (currentVersionSplit[1]);
            int build = Convert.ToInt32 (currentVersionSplit[2]) + 1;

            UnityPlayerSettings.bundleVersion = major + "." + minor + "." + build;

            if (buildTarget == BuildTarget.iOS)
            {
                UnityPlayerSettings.iOS.buildNumber = "" + build + "";
                Console.Log ("Finished with bundleversioncode: " + UnityPlayerSettings.iOS.buildNumber + " and version" + UnityPlayerSettings.bundleVersion);

            }
            else if (buildTarget == BuildTarget.Android)
            {
                UnityPlayerSettings.Android.bundleVersionCode = build;
                Console.Log ("Finished with bundleversioncode: " + UnityPlayerSettings.Android.bundleVersionCode + " and version" + UnityPlayerSettings.bundleVersion);
            }

        }
        catch (Exception e)
        {
            Console.Error (e.Message + "\n" + e.ToString ());
            Console.Error ("AutoIncrementBuildVersion script failed.");
        }
    }
    #endregion
}